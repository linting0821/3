package com.tuyano.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.tuyano.springboot.LoginUserService;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private LoginUserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/login", "/login-error","/css/**", "/images/**", "/js/**").permitAll().anyRequest().authenticated()
                //.antMatchers("/**").hasRole("USER")
                .and()
            .formLogin()
                .loginPage("/login").failureUrl("/login-error");
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//            .userDetailsService(userService)
//            .passwordEncoder(passwordEncoder());
//
//        userService.registerAdmin(100010, "admin", "youmustchangethis", "admin@localhost");
//    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userService)
            .passwordEncoder(passwordEncoder());

        userService.registerAdmin(20190001, "山田太郎", "12345678", "20190001@e-library.com");
        userService.registerAdmin(20190002, "伊藤二郎", "12345679", "20190002@e-library.com");
        userService.registerAdmin(20190003, "山口花子", "12345680", "20190003@e-library.com");
        userService.registerAdmin(20190004, "佐藤沙絵", "12345681", "20190004@e-library.com");
        userService.registerAdmin(20190005, "花田長平", "12345682", "20190005@e-library.com");
        userService.registerAdmin(20190006, "嶋田菜名", "12345683", "20190006@e-library.com");
        userService.registerAdmin(20190007, "森沢信貴", "12345684", "20190007@e-library.com");
        userService.registerAdmin(20190008, "瀧澤洋介", "12345685", "20190008@e-library.com");
        userService.registerAdmin(20190009, "木村彰", "12345686", "20190009@e-library.com");
        userService.registerAdmin(20190010, "長嶋修司", "12345687", "20190010@e-library.com");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}