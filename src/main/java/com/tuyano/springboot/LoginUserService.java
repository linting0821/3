package com.tuyano.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginUserService implements UserDetailsService {

    @Autowired
    private LoginUserRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;
//
//
//		// TODO Auto-generated method stub
//		return null;
//	}
    
    
	public LoginUserData loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null || "".equals(username)) {
            throw new UsernameNotFoundException("Username is empty");
        }

        LoginUserData user = repository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found: " + username);
        }

        return user;
    }
//
    @Transactional
    public void registerAdmin(Integer employeeNumber, String username, String password, String mailAddress) {
        LoginUserData user = new LoginUserData(employeeNumber, username, passwordEncoder.encode(password), mailAddress);
        user.setAdmin(true);
        repository.save(user);
    }

    @Transactional
    public void registerUser(Integer employeeNumber, String username, String password, String mailAddress) {
        LoginUserData user = new LoginUserData(employeeNumber, username, passwordEncoder.encode(password), mailAddress);
        user.setAdmin(false);
        repository.save(user);
    }
}