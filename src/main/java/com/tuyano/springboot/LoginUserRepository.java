package com.tuyano.springboot;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUserData, Long> {

    public LoginUserData findByUsername(String username);

    public LoginUserData findByMailAddress(String mailAddress);

}