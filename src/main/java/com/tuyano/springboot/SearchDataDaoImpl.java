package com.tuyano.springboot;
//検索に必要な本のデータの追加
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;


	public class SearchDataDaoImpl implements SearchDataDao<SearchData> {
		
		private static final long serialVersionUID = 1L;
	    private EntityManager entityManager;
	
	    public SearchDataDaoImpl(){
		super();
	}
	    public SearchDataDaoImpl(EntityManager manager){
		this();
		entityManager = manager;
	}
	
	
	public List<SearchData> getAll() {
		Query query = entityManager.createQuery("from SearchData");
		@SuppressWarnings("unchecked")
		List<SearchData> list = query.getResultList();
		entityManager.close();
		return list;
	}
	
	//ID（行番号）での検索
	public SearchData findById(long id) {
		return (SearchData)entityManager.createQuery("from SearchData where id = "+ id).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public List<SearchData> find(String fstr){
		List<SearchData> list = null;
		String qstr = "from SearchData where id = :fid or name like :fname";
		Long fid = 0L;
		try {
			fid = Long.parseLong(fstr);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		Query query = entityManager.createQuery(qstr).setParameter("fid", fid)
				.setParameter("fname", "%" + fstr + "%");
		list = query.getResultList();
		return list;
	}
	
}

