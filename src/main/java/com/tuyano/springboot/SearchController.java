package com.tuyano.springboot;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


import com.tuyano.springboot.repositories.SearchDataRepository;

@Controller
public class SearchController {
	
	@Autowired
	SearchDataRepository repository;
	
	@PersistenceContext
	EntityManager entityManager;
	SearchDataDaoImpl dao; 
	
	@PostConstruct
	public void init() {
     dao = new SearchDataDaoImpl(entityManager);
    
	// book id 1
	SearchData d1 = new SearchData();
	d1.setId(1);
	d1.setTypeId("3");
	d1.setTypeName("Sales & Marketing" );
	d1.setKeyword("service");
	d1.setBookName("サービス・デザインの教科書");
	d1.setAuthor("武山政直");
	d1.setPublisher("NTT出版社");
	d1.setPrice(4000);
	repository.saveAndFlush(d1);
	//book id 2
	SearchData d2 = new SearchData();
	d2.setId(2);
	d2.setTypeId("3");
	d2.setTypeName("Sales & Marketing" );
	d2.setKeyword("accountancy");
	d2.setBookName("みんなが欲しかった簿記の教科書日商簿記3級");
	d2.setAuthor("瀧澤ななみ");
	d2.setPublisher("TAC出版");
	d2.setPrice(950);
	repository.saveAndFlush(d2);

	//book id 3
	SearchData d3 = new SearchData();
	d3.setId(3);
	d3.setTypeId("2");
	d3.setTypeName("Java技能" );
	d3.setKeyword("shopping");
	d3.setBookName("新・頑張る商店街77選");
	d3.setAuthor("中小企業診断士");
	d3.setPublisher("中小企業庁");
	d3.setPrice(5000);
	repository.saveAndFlush(d3);

	//book id 4
	SearchData d4 = new SearchData();
	d4.setId(4);
	d4.setTypeId("1");
	d4.setTypeName("Microsoft Office" );
	d4.setKeyword("excel");
	d4.setBookName("e-servicescape");
	d4.setAuthor("Seonjeong Lee");
	d4.setPublisher("Emerald");
	d4.setPrice(1200);
	repository.saveAndFlush(d4);

	//book id 5
	SearchData d5 = new SearchData();
	d5.setId(5);
	d5.setTypeId("2");
	d5.setTypeName("Java技能" );
	d5.setKeyword("RPA");
	d5.setBookName("図解入門 最新 RPAがよくわかる本");
	d5.setAuthor("西村泰洋");
	d5.setPublisher("秀和システム");
	d5.setPrice(1728);
	repository.saveAndFlush(d5);

	//book id 6
	SearchData d6 = new SearchData();
	d6.setId(6);
	d6.setTypeId("2");
	d6.setTypeName("Java技能" );
	d6.setKeyword("RPA");
	d6.setBookName("RPAの威力 ~ロボットと共に生きる働き方改革~");
	d6.setAuthor("安部慶喜&金弘潤一郎");
	d6.setPublisher("日経BP社");
	d6.setPrice(1944);
	repository.saveAndFlush(d6);

	//book id 7
	SearchData d7 = new SearchData();
	d7.setId(7);
	d7.setTypeId("4");
	d7.setTypeName("自己啓発" );
	d7.setKeyword("self");
	d7.setBookName("従業員満足度");
	d7.setAuthor("鈴木研一");
	d7.setPublisher("日本管理会");
	d7.setPrice(2800);
	repository.saveAndFlush(d7);

	//book id 8
	SearchData d8 = new SearchData();
	d8.setId(8);
	d8.setTypeId("4");
	d8.setTypeName("自己啓発" );
	d8.setKeyword("KPI");
	d8.setBookName("人事評価で業績をあげるA4一枚評価制度");
	d8.setAuthor("榎本あつし");
	d8.setPublisher("アニモ出版");
	d8.setPrice(2160);
	repository.saveAndFlush(d8);

	//book id 9
	SearchData d9 = new SearchData();
	d9.setId(9);
	d9.setTypeId("4");
	d9.setTypeName("自己啓発" );
	d9.setKeyword("self");
	d9.setBookName("嫌われる勇気　自己啓発の源流「アドラー」の教え");
	d9.setAuthor("岸見一郎");
	d9.setPublisher("ダイヤモンド社");
	d9.setPrice(1620);
	repository.saveAndFlush(d9);

	//book id 10
	SearchData d10 = new SearchData();
	d10.setId(10);
	d10.setTypeId("1");
	d10.setTypeName("Microsoft Office" );
	d10.setKeyword("excel");
	d10.setBookName("Microsoft Office Specialist Microsoft Excel 2016 対策テキスト& 問題集 (よくわかるマスター)");
	d10.setAuthor("富士通エフ・オー・エム");
	d10.setPublisher("富士通エフ・オー・エム");
	d10.setPrice(2160);
	repository.saveAndFlush(d10);

	//book id 11
	SearchData d11 = new SearchData();
	d11.setId(11);
	d11.setTypeId("4");
	d11.setTypeName("自己啓発" );
	d11.setKeyword("English");
	d11.setBookName("どんどん話すための瞬間英作文トレーニング");
	d11.setAuthor("森沢洋介");
	d11.setPublisher("ベレ出版");
	d11.setPrice(1944);
	repository.saveAndFlush(d11);

	//book id 12
	SearchData d12 = new SearchData();
	d12.setId(12);
	d12.setTypeId("4");
	d12.setTypeName("自己啓発" );
	d12.setKeyword("self");
	d12.setBookName("才能の正体");
	d12.setAuthor("坪田信貴");
	d12.setPublisher("幻冬舎");
	d12.setPrice(1620);
	repository.saveAndFlush(d12);

	//book id 13
	SearchData d13 = new SearchData();
	d13.setId(13);
	d13.setTypeId("2");
	d13.setTypeName("Java技能" );
	d13.setKeyword("3DOG");
	d13.setBookName("Blender 3DCG モデリング・マスター");
	d13.setAuthor("Benamin");
	d13.setPublisher("ソーテック社");
	d13.setPrice(3024);
	repository.saveAndFlush(d13);

	//book id 14
	SearchData d14 = new SearchData();
	d14.setId(14);
	d14.setTypeId("2");
	d14.setTypeName("Java技能" );
	d14.setKeyword("AI");
	d14.setBookName("AI vs. 教科書が読めない子どもたち");
	d14.setAuthor("新井紀子");
	d14.setPublisher("東洋経済新報社");
	d14.setPrice(1500);
	repository.saveAndFlush(d14);

	//book id 15
	SearchData d15 = new SearchData();
	d15.setId(15);
	d15.setTypeId("2");
	d15.setTypeName("Java技能" );
	d15.setKeyword("SAP");
	d15.setBookName("図解入門 よくわかる最新SAPの導入と運用 (How-nual図解入門Visual Guide Book)");
	d15.setAuthor("村上均");
	d15.setPublisher("秀和システム");
	d15.setPrice(2376);
	repository.saveAndFlush(d15);

	//book id 16
	SearchData d16 = new SearchData();
	d16.setId(16);
	d16.setTypeId("3");
	d16.setTypeName("Sales & Marketing" );
	d16.setKeyword("accountancy");
	d16.setBookName("みんなが欲しかった 簿記の教科書 日商2級 商業簿記 第8版");
	d16.setAuthor("瀧澤ななみ");
	d16.setPublisher("TAC出版");
	d16.setPrice(1080);
	repository.saveAndFlush(d16);

	//book id 17
	SearchData d17 = new SearchData();
	d17.setId(17);
	d17.setTypeId("3");
	d17.setTypeName("Sales & Marketing" );
	d17.setKeyword("accountancy");
	d17.setBookName("スッキリとける日商簿記2級　過去+予想問題集");
	d17.setAuthor("瀧澤ななみ");
	d17.setPublisher("TAC出版");
	d17.setPrice(1512);
	repository.saveAndFlush(d17);

	//book id 18
	SearchData d18 = new SearchData();
	d18.setId(18);
	d18.setTypeId("2");
	d18.setTypeName("Java技能" );
	d18.setKeyword("html");
	d18.setBookName("スラスラわかるHTML&CSSのきほん 第2版");
	d18.setAuthor("狩野祐東");
	d18.setPublisher("SBクリエイティブ");
	d18.setPrice(2138);
	repository.saveAndFlush(d18);

	//book id 19
	SearchData d19 = new SearchData();
	d19.setId(19);
	d19.setTypeId("4");
	d19.setTypeName("自己啓発" );
	d19.setKeyword("self");
	d19.setBookName("王とサーカス");
	d19.setAuthor("米澤穂信");
	d19.setPublisher("東京創元社");
	d19.setPrice(929);
	repository.saveAndFlush(d19);

	//book id 20
	SearchData d20 = new SearchData();
	d20.setId(20);
	d20.setTypeId("4");
	d20.setTypeName("自己啓発" );
	d20.setKeyword("self");
	d20.setBookName("いまさら翼といわれても");
	d20.setAuthor("米澤穂信");
	d20.setPublisher("KADOKAWA");
	d19.setPrice(734);
	repository.saveAndFlush(d20);

	//book id 21
	SearchData d21 = new SearchData();
	d21.setId(21);
	d21.setTypeId("4");
	d21.setTypeName("自己啓発" );
	d21.setKeyword("English");
	d21.setBookName("TOEIC L & R TEST 出る単特急 金のフレーズ (TOEIC TEST 特急シリーズ)");
	d21.setAuthor("TEX加藤");
	d21.setPublisher("朝日新聞出版");
	d21.setPrice(961);
	repository.saveAndFlush(d21);

	//book id 22
	SearchData d22 = new SearchData();
	d22.setId(22);
	d22.setTypeId("4");
	d22.setTypeName("自己啓発" );
	d22.setKeyword("English");
	d22.setBookName("Mr. Evine の中学英文法を修了するドリル");
	d22.setAuthor("Evine");
	d22.setPublisher("アルク");
	d22.setPrice(1836);
	repository.saveAndFlush(d22);

	//book id 23
	SearchData d23 = new SearchData();
	d23.setId(23);
	d23.setTypeId("1");
	d23.setTypeName("Microsoft Office" );
	d23.setKeyword("excel");
	d23.setBookName("Excel 最強の教科書[完全版]");
	d23.setAuthor("藤井直弥");
	d23.setPublisher("SBクリエイティブ");
	d23.setPrice(1706);
	repository.saveAndFlush(d23);

	//book id 24
	SearchData d24 = new SearchData();
	d24.setId(24);
	d24.setTypeId("2");
	d24.setTypeName("Java技能" );
	d24.setKeyword("Linux");
	d24.setBookName("LinuxコマンドABCリファレンス");
	d24.setAuthor("中島能和");
	d24.setPublisher("翔泳社");
	d24.setPrice(2618);
	repository.saveAndFlush(d24);

	//book id 25
	SearchData d25 = new SearchData();
	d25.setId(25);
	d25.setTypeId("4");
	d25.setTypeName("自己啓発" );
	d25.setKeyword("self");
	d25.setBookName("ミレニアムと私");
	d25.setAuthor("エヴァ・ガブリエルソン");
	d25.setPublisher("早川書房");
	d25.setPrice(1760);
	repository.saveAndFlush(d25);


	//book id 26
	SearchData d26 = new SearchData();
	d26.setId(26);
    d26.setTypeId("1");
	d26.setTypeName("Microsoft Office" );
	d26.setKeyword("excel");
	d26.setBookName("たった1秒で仕事が片づくExcel自動化の教科書【増強完全版】");
	d26.setAuthor("吉田拳");
	d26.setPublisher("技術評論社");
	d26.setPrice(1800);
	repository.saveAndFlush(d26);
	
	}

		@RequestMapping(value = "/find", method = RequestMethod.GET)
	    public ModelAndView find(ModelAndView mav) {
	    	mav.setViewName("find");
	    	mav.addObject("title","");
	    	mav.addObject("");
	    	mav.addObject("value","");
	    	Iterable<SearchData> list = dao.getAll(); //●
	    	mav.addObject("datalist", list);
	    	return mav;
	    }

	    @RequestMapping(value = "/find", method = RequestMethod.POST)
	    public ModelAndView search(HttpServletRequest request,
	    		ModelAndView mav) {
	    	mav.setViewName("find");
	    	String param = request.getParameter("fstr");
	    	if (param == ""){
	    		mav = new ModelAndView("redirect:/find");
	    	} else {
	    		mav.addObject("title","Find result");
	    		mav.addObject("msg","「" + param + "」の検索結果");
	    		mav.addObject("value",param);
	    		List<SearchData> list = dao.find(param);
	    		mav.addObject("datalist", list);
	    	}
	    	return mav;
	    }
}


	   
