package com.tuyano.springboot;
//本のデータの取得
import java.io.Serializable;
import java.util.List;

public interface SearchDataDao <T> extends Serializable {
	
	
	public List<T> getAll();
	public T findById(long id); 
	public List<T> find(String fstr);

	
}