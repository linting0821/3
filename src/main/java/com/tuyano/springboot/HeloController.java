package com.tuyano.springboot;

import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class HeloController {
	

//	@RequestMapping("/")
//	public ModelAndView index(ModelAndView mav) {
//		mav.setViewName("index");
//		return mav;
//	}
	
	@RequestMapping("/top")
	public ModelAndView top(ModelAndView mav) {
		mav.setViewName("top");
		return mav;
	}
	
	@RequestMapping("/detail")
	public ModelAndView detail(ModelAndView mav) {
		mav.setViewName("detail");
		return mav;
	}
	
	@RequestMapping("/office")
	public ModelAndView office(ModelAndView mav) {
		mav.setViewName("office");
		return mav;
	}
	
	@RequestMapping("/java")
	public ModelAndView java(ModelAndView mav) {
		mav.setViewName("java");
		return mav;
	}
	
	@RequestMapping("/marketing")
	public ModelAndView marketing(ModelAndView mav) {
		mav.setViewName("marketing");
		return mav;
	}
	
	@RequestMapping("/inspire")
	public ModelAndView inspire(ModelAndView mav) {
		mav.setViewName("inspire");
		return mav;
	}

	@RequestMapping("/search")
	public ModelAndView search(ModelAndView mav) {
		mav.setViewName("search");
		return mav;
	}
	
//	@RequestMapping(value="/", method=RequestMethod.GET)
//	public ModelAndView index1(ModelAndView mav) {
//		mav.setViewName("index");
//		mav.addObject("msg","社員情報です。");
//		Iterable<EmployeeData> list=dao.getAll();
//		mav.addObject("datalist", list);
//		return mav;
//	}
}
