package com.tuyano.springboot.repositories;
//本のデータリポジトリ
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.tuyano.springboot.SearchData;

@Repository
public interface SearchDataRepository  extends JpaRepository<SearchData, Long>{ 

}
