package com.tuyano.springboot;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginAuthController {

    @Autowired
    LoginUserService userService;
    
//    @GetMapping("/signup")
//    public String signup(Model model) {
//        model.addAttribute("signupForm", new SignupForm());
//        return "signup";
//    }
//
//    @PostMapping("/signup")
//    public String signupPost(Model model, @Valid SignupForm signupForm,
//    		BindingResult bindingResult, HttpServletRequest request) {
//    	
//        if (bindingResult.hasErrors()) {
//            return "signup";
//        }
//
//        try {
//            userService.registerUser(signupForm.getUsername(),
//            		signupForm.getPassword(), signupForm.getMailAddress());
//        } catch (DataIntegrityViolationException e) {
//            model.addAttribute("signupError", true);
//            return "signup";
//        }
//
//        try {
//            request.login(signupForm.getUsername(), signupForm.getPassword());
//        } catch (ServletException e) {
//            e.printStackTrace();
//        }
//
//        return "redirect:/messages";
//    }
//    

    @RequestMapping("/")
    public String inde() {
        return "redirect:/top";
    }
    
    @GetMapping("/login")
    public String index() {
        return "index";
    }

    @PostMapping("/login")
    public String loginPost() {
        return "/top";
    }

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "index";
    }
    

    @GetMapping("/top")
    public String top() {
        return "top";
    }
    
    

}